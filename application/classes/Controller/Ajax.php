<?php defined('SYSPATH') or die('No direct script access.');
/**
 *This Class is used for all ajax call in the image app 
 *
 *
 *@Namespace Application\Classes
 *
 *@package Controller
 *
 * @author Ritchie Prades
 */
class Controller_Ajax extends Controller {

	public function action_index()
	{
		//list down content
        $this->action_read();
	}
    
    public function action_add(){
        $post = $_POST;
        if(!trim($post['title'])){
            echo json_encode(array("message"=>"Title Required","status"=>"error"));
            exit;
        }
      if($_FILES){
       $filename = $this->_save_image($_FILES['image']);
       if($filename){
           //success
            $images = ORM::factory('images');
            $images->title = $post['title'];
            //$images->filename = $_FILES['image']['name'];
            $images->filename = $filename;
            $images->save();
            echo json_encode(array("message"=>"Success!","status"=>"success"));
            exit;
       }else{
            echo json_encode(array("message"=>"Please upload a valid image!","status"=>"error"));
            exit;
       }
        }else{
            //show an error
            echo json_encode(array("message"=>"Image Required","status"=>"error"));
            exit;
        }
    }
    
    public function action_delete(){
        $id = $_POST['id'];
        $Image = ORM::factory('Images', $id);
        
        if ($Image->loaded())
        {
            unlink("assets/uploads/".$Image->filename);
        }
        $Image->delete();
    }
    
    public function action_read(){
        $images = ORM::factory('images')->find_all();
                $data = '<table class="table table-bordered table-striped">
                                <tr>
                                    <th>No.</th>
                                     <th>Image</th>
                                    <th>Title</th>
                                    <th>Filename</th>
                                    <th>Date Added</th>
                                    <th>Action</th>
                                </tr>';
                // if query results contains rows then featch those rows 
            if(count($images) > 0)
            {
                foreach($images as $image)
                {
                    $data .= '<tr id="tr_'.$image->id.'">
                        <td>'.$image->id.'</td>
                        <td><img class="thumb-center" src='.url::site()."assets/uploads/".$image->filename.' ></td>
                        <td>'.ucwords($image->title).'</td>
                        <td>'.$image->filename.'</td>
                        <td>'.$image->data_added.'</td>
                        <td>
                            <button onclick="GetDetails('.$image->id.')" class="btn btn-warning">Update</button>
                            <button onclick="Delete('.$image->id.')" class="btn btn-danger">Delete</button>
                        </td>
                    </tr>';
                }
            }
            else
            {
                // records now found 
                $data .= '<tr><td colspan="6">Records not found!</td></tr>';
            }

    $data .= '</table>';

    echo $data;
  
    }
    
    public function action_readdetails(){
         $id = $_POST;
        $image = ORM::factory('images', $id);
        if ($image->loaded())
        {
            $data = array(
                "id"=>$image->id,
                "title"=>$image->title,
                "filename"=>$image->filename
            );
            echo json_encode($data);
        }
        else
        {
           echo false;
        }
    }
    
    public function action_update(){
        $post = $_POST;
         if(!trim($post['title'])){ //truncate
            echo json_encode(array("message"=>"Title Required","status"=>"error"));
            exit;
        }
        $image = ORM::factory('images', $post['id']);
        if ($image->loaded())
        {
            $image->title = $post["title"];
            if($_FILES){
                $filename = $this->_save_image($_FILES['image']);
                if($filename){
                    $image->filename = $filename;
                }else{
                echo json_encode(array("message"=>"Please upload a valid image!","status"=>"error"));
                exit;
                }
            }
            $image->save();           
            echo json_encode(array("message"=>"Success!","status"=>"success"));
            exit;
        }else{
            //show an error
            echo json_encode(array("message"=>"There is something wrong in your request","status"=>"error"));
            exit;
        }
    }
    protected function _save_image($image)
    {
        if (
            ! Upload::valid($image) OR
            ! Upload::not_empty($image) OR
            ! Upload::type($image, array('jpg', 'jpeg', 'png', 'gif')))
        {
            return FALSE;
        }
 
        $directory = DOCROOT.'assets/uploads/';
 
        if ($file = Upload::save($image, NULL, $directory))
        {
            $filename = strtolower(Text::random('alnum', 20)).'.jpg';
 
            Image::factory($file)
               // ->resize(200, 200, Image::AUTO)
                ->save($directory.$filename);
 
            // Delete the temporary file
            unlink($file);
            return $filename;
        }
        return FALSE;
    }
} // End Ajax

