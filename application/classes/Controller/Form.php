<?php defined('SYSPATH') or die('No direct script access.');
/**
 *This Class is the initial view that was used
 *
 *
 *@Namespace Application\Classes
 *
 *@package Controller
 *
 * @author Ritchie Prades
 */
class Controller_Form extends Controller_Template {
    public $template = 'form';
    public $page_title;
 
    public function before()
    {
        parent::before();
        // Make $page_title available to all views
        View::bind_global('page_title', $this->page_title);
       
    }
    
	public function action_index()
	{
		$this->page_title = 'Home';
         $this->template->content = View::factory('template/list');
	}

} // End Welcome
