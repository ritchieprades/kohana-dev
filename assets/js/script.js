!function(e){var t=function(t,n){this.$element=e(t),this.type=this.$element.data("uploadtype")||(this.$element.find(".thumbnail").length>0?"image":"file"),this.$input=this.$element.find(":file");if(this.$input.length===0)return;this.name=this.$input.attr("name")||n.name,this.$hidden=this.$element.find('input[type=hidden][name="'+this.name+'"]'),this.$hidden.length===0&&(this.$hidden=e('<input type="hidden" />'),this.$element.prepend(this.$hidden)),this.$preview=this.$element.find(".fileupload-preview");var r=this.$preview.css("height");this.$preview.css("display")!="inline"&&r!="0px"&&r!="none"&&this.$preview.css("line-height",r),this.original={exists:this.$element.hasClass("fileupload-exists"),preview:this.$preview.html(),hiddenVal:this.$hidden.val()},this.$remove=this.$element.find('[data-dismiss="fileupload"]'),this.$element.find('[data-trigger="fileupload"]').on("click.fileupload",e.proxy(this.trigger,this)),this.listen()};t.prototype={listen:function(){this.$input.on("change.fileupload",e.proxy(this.change,this)),e(this.$input[0].form).on("reset.fileupload",e.proxy(this.reset,this)),this.$remove&&this.$remove.on("click.fileupload",e.proxy(this.clear,this))},change:function(e,t){if(t==="clear")return;var n=e.target.files!==undefined?e.target.files[0]:e.target.value?{name:e.target.value.replace(/^.+\\/,"")}:null;if(!n){this.clear();return}this.$hidden.val(""),this.$hidden.attr("name",""),this.$input.attr("name",this.name);if(this.type==="image"&&this.$preview.length>0&&(typeof n.type!="undefined"?n.type.match("image.*"):n.name.match(/\.(gif|png|jpe?g)$/i))&&typeof FileReader!="undefined"){var r=new FileReader,i=this.$preview,s=this.$element;r.onload=function(e){i.html('<img src="'+e.target.result+'" '+(i.css("max-height")!="none"?'style="max-height: '+i.css("max-height")+';"':"")+" />"),s.addClass("fileupload-exists").removeClass("fileupload-new")},r.readAsDataURL(n)}else this.$preview.text(n.name),this.$element.addClass("fileupload-exists").removeClass("fileupload-new")},clear:function(e){this.$hidden.val(""),this.$hidden.attr("name",this.name),this.$input.attr("name","");if(navigator.userAgent.match(/msie/i)){var t=this.$input.clone(!0);this.$input.after(t),this.$input.remove(),this.$input=t}else this.$input.val("");this.$preview.html(""),this.$element.addClass("fileupload-new").removeClass("fileupload-exists"),e&&(this.$input.trigger("change",["clear"]),e.preventDefault())},reset:function(e){this.clear(),this.$hidden.val(this.original.hiddenVal),this.$preview.html(this.original.preview),this.original.exists?this.$element.addClass("fileupload-exists").removeClass("fileupload-new"):this.$element.addClass("fileupload-new").removeClass("fileupload-exists")},trigger:function(e){this.$input.trigger("click"),e.preventDefault()}},e.fn.fileupload=function(n){return this.each(function(){var r=e(this),i=r.data("fileupload");i||r.data("fileupload",i=new t(this,n)),typeof n=="string"&&i[n]()})},e.fn.fileupload.Constructor=t,e(document).on("click.fileupload.data-api",'[data-provides="fileupload"]',function(t){var n=e(this);if(n.data("fileupload"))return;n.fileupload(n.data());var r=e(t.target).closest('[data-dismiss="fileupload"],[data-trigger="fileupload"]');r.length>0&&(r.trigger("click.fileupload"),t.preventDefault())})}(window.jQuery)

// Add Record
$(".close").html("x");
function addRecord() {
    // get values
    var title = $("#title").val();
    var fileupload = $("#fileupload").prop("files")[0];  
    var data = new FormData();
    data.append('image', $('#fileupload').prop('files')[0]);
    // append other variables to data if you want: data.append('field_name_x', field_value_x);
    data.append('title', title);
 	
    // Add record
   $.ajax({
        type: "POST",
        dataType: "json",
        data: data,
        url: "index.php/ajax/add", 
        cache: false,
        processData: false, 
        contentType: false,
        success: function(data) {
            if(data.status=="success"){
                $("#add_new_record_modal").modal("hide");
                // read records again
                readRecords();
                 $("#title").val("");
                $("#fileupload").val("");
                $(".fileupload-exists").trigger("click");
                $("#new_error_message").hide();
            }else{
                $("#new_error_message").show();
                $("#new_error_message").html("ERROR: "+ data.message);
            }
            
        },
        error: function() {
            $("#new_error_message").show();
            $("#new_error_message").html("ERROR: There is something wrong will processing your request");
             $("#title").val("");
            $("#fileupload").val("");
            $(".fileupload-exists").trigger("click");
        }
	});
}

// READ records
function readRecords() {
    $.get("index.php/ajax/read", {}, function (data, status) {
        $(".records_content").html(data);
    });
}

function Delete(id) {
    var conf = confirm("Are you sure, do you really want the image?");
    if (conf == true) {
        $("#tr_"+id).fadeOut("slow");
        setTimeout(function(){
            $.post("index.php/ajax/delete", {
                    id: id
                },
                function (data, status) {
                    // reload data by using readRecords();
                    readRecords();
                }
            );
        },1000);
    }
}

function GetDetails(id) {
    // Add image ID to the hidden field for furture usage
    $("#hidden_id").val(id);
    var data = new Object();
    data.id = id;
    $.ajax({
        type: "POST",
       dataType: "json",
        data: data,
        url: "index.php/ajax/readdetails", 
        success: function(data) {
            $("#update_title").val(data.title);
            $("#update_image").attr("src", "assets/uploads/"+ data.filename);
        },
        error: function() {
         
        }
	});
    
    // Open modal popup
    $("#update_file_modal").modal("show");
}

function UpdateDetails() {
    // get values
    var title = $("#update_title").val();
    var id = $("#hidden_id").val();
    var data = new FormData();
    data.append('image', $('#fileupload_update').prop('files')[0]);
    // append other variables to data if you want: data.append('field_name_x', field_value_x);
    data.append('title', title);
    // get hidden field value
    data.append('id',id);

    // Update the details by requesting to the server using ajax
    $.ajax({
        type: "POST",
        dataType: "json",
        data: data,
        url: "index.php/ajax/update", 
        cache: false,
        processData: false, 
        contentType: false, 
        success: function(data) {
             if(data.status=="success"){
             // hide modal popup
            $("#update_file_modal").modal("hide");
            $("#update_title").val('');
            $("#hidden_id").val('');
            $(".fileupload-exists").trigger("click");
            // reload by using readRecords();
            readRecords();
            $("#update_error_message").hide();
             }else{
                  $("#update_error_message").show();
                $("#update_error_message").html("ERROR: "+ data.message);
             }
        },
        error: function() {
           $("#update_error_message").show();
            $("#update_error_message").html("ERROR: There is something wrong will processing your request");
        }
	});
    
}

$(document).ready(function () {
    // READ recods on page load
    readRecords(); // calling function
});